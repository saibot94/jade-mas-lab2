package agentparty;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

/**
 * Created by darkg on 21-May-17.
 */
public class Host extends Agent {

    private static final String[] rumours = {"rumour1", "rumour2", "rumour3", "rumour4", "rumour5"};
    private Set<AID> agentsWithRumors = new HashSet<>();

    @Override
    protected void setup() {
        System.out.println(getLocalName() + " STARTED");
        Integer guests = getNumberOfGuests();
        if (guests == null) {
            System.out.println("Invalid guests");
            doDelete();
        } else {
            registerDf();
            createAgents(guests);
            try {
                Thread.sleep(3000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            addBehaviours();
        }
    }

    private void addBehaviours() {
        // Select one guest at random and then tell it the rumor
        // Then select some other two agents at random and introduce them to one another
        addBehaviour(new OneShotBehaviour() {
            @Override
            public void action() {
                ACLMessage rumorMessage = new ACLMessage(ACLMessage.PROPOSE);
                ACLMessage greetMessage = new ACLMessage(ACLMessage.INFORM);
                String randomRumour = rumours[ThreadLocalRandom.current().nextInt(0, rumours.length)];
                rumorMessage.setContent(randomRumour);

                List<DFAgentDescription> agents = getAgents(myAgent);
                AID agent1 = agents.get(ThreadLocalRandom.current().nextInt(0, agents.size())).getName();
                AID agent2 = agents.get(ThreadLocalRandom.current().nextInt(0, agents.size() )).getName();
                AID agentRumor = agents.get(ThreadLocalRandom.current().nextInt(0, agents.size() )).getName();

                greetMessage.addReceiver(agent1);
                greetMessage.setContent(agent2.getName());
                send(greetMessage);

                greetMessage.clearAllReceiver();
                greetMessage.addReceiver(agent2);
                greetMessage.setContent(agent1.getName());
                send(greetMessage);

                rumorMessage.addReceiver(agentRumor);
                send(rumorMessage);


            }
        });


        // Get notifications from agents, acknowledging that they know the rumour
        addBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {
                ACLMessage message = receive(MessageTemplate.MatchPerformative(ACLMessage.CONFIRM));
                if(message != null ) {
                    agentsWithRumors.add(message.getSender());
                    List<DFAgentDescription> agents = getAgents(myAgent);
                    if(agentsWithRumors.containsAll(agents.parallelStream().map(DFAgentDescription::getName).collect(Collectors.toList()))){
                        System.out.println("All other agents know the rumors, SHUTTING DOWN!!!");
                        doDelete();
                    }
                } else {
                    block();
                }
            }
        });

        // Get notifications from agents, asking them to be introduced to another agent.
        addBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {
                ACLMessage message = receive(MessageTemplate.MatchPerformative(ACLMessage.REQUEST));
                if(message != null) {
                    List<DFAgentDescription> agents = getAgents(myAgent);
                    int nextAgentToIntroduce = ThreadLocalRandom.current().nextInt(0, agents.size() );
                    ACLMessage reply = message.createReply();
                    reply.setPerformative(ACLMessage.INFORM);
                    reply.setContent(agents.get(nextAgentToIntroduce).getName().getName());
                    send(reply);
                } else {
                    block();
                }
            }
        });
    }

    private List<DFAgentDescription> getAgents(Agent myAgent) {
        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType("party-guest");
        template.addServices(sd);

        DFAgentDescription[] result;
        try {
            result = DFService.search(myAgent, template);
            return Arrays.asList(result);
        } catch (FIPAException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    private void createAgents(Integer agentNo) {
        // create agent t1 on the same container of the creator agent
        AgentContainer container = getContainerController(); // get a container controller for creating new agents
        for (int i = 0; i < agentNo; i++) {
            try {
                String agentName = new StringBuilder().append("guest").append(String.valueOf(i)).toString();
                AgentController t1 = container.createNewAgent(agentName, "agentparty.Guest", null);
                t1.start();
                System.out.println(getLocalName() + " CREATED AND STARTED NEW GUEST: " + agentName + " ON CONTAINER " + container.getContainerName());
            } catch (Exception any) {
                any.printStackTrace();
            }
        }
    }

    private void registerDf() {
        try {
            // create the agent descrption of itself
            DFAgentDescription dfd = new DFAgentDescription();
            dfd.setName(getAID());
            ServiceDescription sd = new ServiceDescription();
            sd.setType("party-host");
            sd.setName("JADE-party-hosts");
            dfd.addServices(sd);
            // register the description with the DF
            DFService.register(this, dfd);
            System.out.println(getLocalName() + " REGISTERED WITH THE DF");
        } catch (FIPAException e) {
            e.printStackTrace();
        }
    }

    private Integer getNumberOfGuests() {
        Object[] args = getArguments();
        if (args != null && args.length > 0) {
            return Integer.parseInt((String) args[0]);
        }
        return null;
    }
}
