package agentparty;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static jade.lang.acl.MessageTemplate.MatchPerformative;

/**
 * Created by darkg on 21-May-17.
 */
public class Guest extends Agent {

    private String rumor = null;
    @Override
    protected void setup() {
        System.out.println(getLocalName() + " STARTED");
        registerOnDf();
        addBehaviours();
    }

    // Behaviours for getting messages from host or from
    private void addBehaviours() {
        addBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {
                ACLMessage message = receive(MessageTemplate.or(
                        MatchPerformative(ACLMessage.PROPOSE),
                        MatchPerformative(ACLMessage.INFORM)
                ));
                if(message != null) {
                    // Get rumor, if it is not known and heard for the first time, inform the host
                    if (message.getPerformative() == ACLMessage.PROPOSE) {
                        if (rumor == null) {
                            rumor = message.getContent();
                            System.out.println(getName() + " I have found out the rumor: " + rumor);
                            // Confirm that you learned about the rumor
                            ACLMessage hostInformMessage = new ACLMessage(ACLMessage.CONFIRM);
                            AID host = getHost(myAgent);
                            hostInformMessage.setContent(myAgent.getAID().toString());
                            if(host != null) {
                                hostInformMessage.addReceiver(host);
                                send(hostInformMessage);
                            }

                        }
                    }
                    // Get other host, send it the rumor if you have it
                    else if (message.getPerformative() == ACLMessage.INFORM) {
                        if(rumor != null) {
                            ACLMessage rumorMessage = new ACLMessage(ACLMessage.PROPOSE);
                            rumorMessage.setContent(rumor);
                            rumorMessage.addReceiver(getAgentWithName(myAgent, message.getContent()));
                            System.out.println("AGENT " + getName() + " meeting with " + message.getContent());
                            send(rumorMessage);
                        }
                    }
                    ACLMessage requestMeetingMessage = new ACLMessage(ACLMessage.REQUEST);
                    AID host = getHost(myAgent);
                    if(host != null) {
                        requestMeetingMessage.addReceiver(host);
                        send(requestMeetingMessage);
                    }
                } else {
                    block();
                }
            }
        });


    }

    private AID getHost(Agent myAgent) {
        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType("party-host");
        template.addServices(sd);

        DFAgentDescription[] result;
        try {
            result = DFService.search(myAgent, template);
            return result[0].getName();
        } catch (FIPAException e) {
            e.printStackTrace();
        }
        return null;
    }


    private AID getAgentWithName(Agent myAgent, String name) {
        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType("party-guest");
        template.addServices(sd);

        DFAgentDescription[] result;
        try {
            result = DFService.search(myAgent, template);
            Optional<DFAgentDescription> gottenAid = Arrays.asList(result).parallelStream().filter(r -> r.getName().getName().equals(name)).findFirst();
            if (gottenAid.isPresent()) {
                return gottenAid.get().getName();
            }
        } catch (FIPAException e) {
            e.printStackTrace();
        }
        return null;
    }


    private void registerOnDf() {
        try {
            // create the agent descrption of itself
            DFAgentDescription dfd = new DFAgentDescription();
            dfd.setName(getAID());
            // register the description with the DF
            ServiceDescription sd = new ServiceDescription();
            sd.setType("party-guest");
            sd.setName("JADE-agent-party");
            dfd.addServices(sd);
            DFService.register(this, dfd);
            System.out.println(getLocalName() + " REGISTERED WITH THE DF");
        } catch (FIPAException e) {
            e.printStackTrace();
        }
    }
}
