#!/bin/bash

if [[ "$#" != 3 ]]; then
    echo "Usage: $0 (main container host) (agent name) (number of guests at the party)" >&2
    exit 1
fi

HOST=$1
AGENT=$2
GUESTS=$3
AGENT_CLASS=agentparty.Host

mvn compile exec:java \
    -Dexec.mainClass=jade.Boot \
    -Dexec.args="-container -host $HOST -agents '$AGENT:$AGENT_CLASS($GUESTS)'"
